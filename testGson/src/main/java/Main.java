import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class Main {
    public static void main(String[] args) throws IOException{
        try(Reader reader = new InputStreamReader(Gson.class.getResourceAsStream("/config.json"), "UTF-8")){
            Gson gson = new GsonBuilder().create();
            Person person = gson.fromJson(reader, Person.class);
            System.out.println(person.name );
            System.out.println(person.car.color);
        }
    }
}
